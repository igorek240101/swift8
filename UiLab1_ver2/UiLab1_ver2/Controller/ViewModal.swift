//
//  ViewModal.swift
//  UiLab1_ver2
//
//  Created by Tatiana Chepkova on 18.03.2023.
//

import UIKit

class ViewModal: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func BackPress(_ sender: UIButton) {
        dismiss(animated: true);
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
